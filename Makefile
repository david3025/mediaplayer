CC = g++
CFLAGS = -Wall -g

main: main.o 
	$(CC) $(CFLAGS) main.o -o main

main.o: main.cpp 
	$(CC) $(CFLAGS) -c main.cpp collection.h wav.h

clean:
	rm *.o main