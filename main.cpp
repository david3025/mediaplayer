#include "wav.h"


int main( int argc, char *argv[] ){

	char line[1024];
	char cmd[1024];
	char arg1[1024];
	char arg2[1024];
	char arg3[1024];
	char arg4[1024];

	int args;

	MySongs mp3;

	cout << "\nWelcome to David and Shlomy Music Player"<<endl;
	cout << " \nthos are you options:"<<endl;
	cout << "  1)help" << endl;
	cout <<	"  2)commands(for dummies)\n"<<endl;

	while(1) {
		
		printf(" Music Player> ");
		fflush(stdout);
		
		if(!fgets(line,sizeof(line),stdin)) break;

		if(line[0]=='\n') continue;
		line[strlen(line)-1] = 0;

		args = sscanf(line,"%s %s %s %s %s",cmd,arg1,arg2,arg3,arg4);

		if(args==0) continue;
		if(!strcmp(cmd,"folder")) {
			if(args==2) {
				(mp3.add_folder(arg1));
			} else {
				printf("use: folder <Folder Name>\n");
			}

		}else if(!strcmp(cmd,"song")) {
			if(args==5){
				(mp3.add_song(arg1,arg2,arg3,arg4));
					
			} else {
				printf("use: song <Song Name> <Artist> <Lyrics> <inside a folder(optional)>\n");
			}
		}else if(!strcmp(cmd,"printF")){
			if(args==3){
				(mp3.print_folder_song(arg1,arg2));
			} else {
				printf("use: printF <Folder Name> <Artist>\n");
			}
		}else if(!strcmp(cmd,"printS")){
			if(args==2){
				(mp3.print_songs(arg1));
			} else {
				printf("use: printS <Artist>\n");
			}
		}else if(!strcmp(cmd,"removeF")){
			if(args==2){
				(mp3.remove_folder(arg1));
			} else {
				printf("use: removeF <Folder Name>\n");
			}
		}else if(!strcmp(cmd,"removeS")){
			if(args==3){
				(mp3.remove_song(arg1,arg2));
			} else {
				printf("use: removeF <Song Name> <Folder Name>\n");
			}
		}else if(!strcmp(cmd,"moveS")){
			if(args==4){
				(mp3.move_song(arg1,arg2,arg3));
			} else {
				printf("use: moveS <Song Name> <Target Folder> <Suorce Folder>\n");
			}
		}else if(!strcmp(cmd,"play")){
			if(args==3){
				(mp3.play(arg1,arg2));
			} else {
				printf("use: play <Song Name> <Folder Name>\n");
			}
		}else if(!strcmp(cmd,"help")) {
			printf("Commands are:\n");
			printf("    folder  -Add Folder:	 <Folder Name>\n");
			printf("    song    -Add Song:		 <Song Name> <Artist> <Lyrics> <inside a folder(optional)>\n");
			printf("    printF  -print Folder Song:	 <Folder Name> <Artist>\n");
			printf("    printS  -Print Artist Songs: <Artist>\n");
			printf("    removeF -Remove Folder:	 <Folder Name>\n");
			printf("    removeS -Remove Song:	 <Song Name> <Folder Name>\n");
			printf("    moveS   -Move Song:		 <Song Name> <Target Folder> <Suorce Folder>\n");
			printf("    play    -Play Song:		 <Song Name> <Folder Name>\n");
			printf("    help\n");
			printf("    exit\n");

		} else if(!strcmp(cmd,"commands")) {
			printf("Commands are:\n");
			printf("    folder\n");
			printf("    song\n");
			printf("    printF\n");
			printf("    printS\n");
			printf("    removeF\n");
			printf("    removeS\n");
			printf("    moveS\n");
			printf("    play\n");
			printf("    help\n");
			printf("    exit\n");

		}else if(!strcmp(cmd,"quit")) {
			break;
		} else if(!strcmp(cmd,"exit")) {
			break;
		} else {
			printf("unknown command: %s\n",cmd);
			printf("type 'help' for a list of commands.\n");
		}
	}

	printf("closing Music Player.\n");
	return 0;
}
