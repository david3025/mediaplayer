#include <iostream>
#include "collection.h"
#include <vector>
#include <cstring>
#include<bits/stdc++.h>

using namespace std;
typedef Iterator<Song*,Collection<Song*>> Iterator_type;

void Play_song(string s) {
  int n = s.length();
  char char_array[n + 1];
  strcpy(char_array, s.c_str());

  char buf[BUFSIZ];
  snprintf(buf, sizeof(buf), "canberra-gtk-play -f songs/%s.wav",char_array); 
  system(buf);
}

bool is_song_exist(Collection<Song*> cont, string my_song ,string master_folder) {
    Iterator_type *my_iterator = cont.GetIterator();
    int size = cont.Size();
    for (int i = 0; i < size; i++)
    {
        if((my_iterator)->Current()->song == my_song && 
           (my_iterator)->Current()->folder == master_folder) {
            return 1;
        }
        my_iterator->Next();
    }
    delete my_iterator;
    return 0;
    

}
bool is_folder_exist(vector<string> folders_names, string folder_name) {
    auto it = folders_names.begin();
    int size = folders_names.size();
    for (int i = 0; i < size; i++)
    {
        if(*(it) == folder_name) {
            return 1;
        }
        it++;
    }
    return 0;
}
class MySongs {
    private:

        vector<string> folders_names;
        Collection<Song*> cont;

    public:
        MySongs() {
            folders_names.push_back("root");
        }
        ~MySongs(){};
        bool add_folder(string folder_name) {
            
            if(is_folder_exist(folders_names, folder_name)) {
                cout <<"allready exist !"<<endl;
                return 0;
            }
            
            folders_names.push_back(folder_name);
            cout <<"added folder successfully !"<<endl;
            return 1;
        }
         
    
        bool add_song(string song_name, string artist, string lyrics, string father_folder="root") {
            
            if(!is_folder_exist(folders_names, father_folder)) {
                cout <<"the folder "<<father_folder<<" is not exist!!"<<endl;
                
                return 0;
            }
            else if (is_song_exist(cont, song_name, father_folder))
            {
                cout <<"the song is allready exist in "<< father_folder<<" folder" <<endl;
                
                return 0;
            }
            Song *new_song = new Song(song_name, artist, lyrics, father_folder);
            cont.Add(new_song);
            cout <<"added song successfully: "<<cont.Size()<< " songs in disk"<<endl;
            return 1;

        }
        bool remove_song(string song_name, string folder_name="root") {

            if (!is_folder_exist(folders_names, folder_name)) {
                cout <<"the folder "<<folder_name<<" is not exist!!"<<endl;
                return 0;
            }
            else if (!is_song_exist(cont, song_name, folder_name))
            {
                cout <<"the song is not exist in "<< folder_name<<" folder" <<endl;
                
                return 0;
            }
            Iterator_type *my_iterator = cont.GetIterator();
            int size = cont.Size();
            for (int i = 0; i < size; i++)
            {
                if((my_iterator)->Current()->folder == folder_name && 
                    (my_iterator)->Current()->song == song_name) {
                   (my_iterator)->Remove(); 
                }

                my_iterator->Next();
            }

            delete my_iterator;



            return 1;

        }
        bool remove_folder( string folder_name="root") {

            if(!is_folder_exist(folders_names, folder_name)) {
                cout <<"folder "<<folder_name<<" not exist !"<<endl;
                return 0;
            }
            Iterator_type *my_iterator = cont.GetIterator();
            int size = cont.Size();
            for (int i = 0; i < size; i++)
            {
                if((my_iterator)->Current()->folder == folder_name) {
                   (my_iterator)->Remove(); 
                }

                my_iterator->Next();
            }
            delete my_iterator;

            auto it = folders_names.begin();
            size = folders_names.size();
            for (int i = 0; i < size; i++)
            {
                if(*(it) == folder_name) {
                    folders_names.erase(it);
                    break;
                }
                it++;
            }
            return 1;

        }
        bool print_folder_song(string folder_name, string artist="") {
            Iterator_type *my_iterator = cont.GetIterator();
            int size = cont.Size();
            for (int i = 0; i < size; i++)
            {
                if(my_iterator->Current()->folder == folder_name && ( (my_iterator->Current()->artist == artist) || artist == "")) {
                    
              
                    cout << my_iterator->Current()->song <<endl;
                }
                my_iterator->Next();
            }
            delete my_iterator;
            return 1; 
        }
        void print_songs(string artist="") {


            Iterator_type *my_iterator = cont.GetIterator();
            int size = cont.Size();
            for (int i = 0; i < size; i++)
            {
                if(artist == "") {
                    cout << my_iterator->Current()->song <<endl;
                }

                else if(my_iterator->Current()->artist == artist){
                    cout << my_iterator->Current()->song <<endl;
                }

                my_iterator->Next();
            }
            delete my_iterator;
   
        }
        bool move_song(string song_name, string trg_folder, string src_folder="root") {
            
            if (!is_folder_exist(folders_names, src_folder)) {
                cout <<"the folder "<<src_folder<<" is not exist!!"<<endl;
                return 0;
            }
            else if (!is_song_exist(cont, song_name, src_folder))
            {
                cout <<"the song is not exist in "<< src_folder<<" folder" <<endl;
                
                return 0;
            }
            else if(is_song_exist(cont, song_name, trg_folder)) {
                cout <<"the song allready exist in "<< trg_folder<<" folder" <<endl;
                
                return 0;
            }
            Iterator_type *my_iterator = cont.GetIterator();
            int size = cont.Size();
            for (int i = 0; i < size; i++)
            {
                if((my_iterator)->Current()->song == song_name && (my_iterator)->Current()->folder == src_folder) {
                    (my_iterator)->Current()->folder = trg_folder;
                    break;
                }

                my_iterator->Next();
            }

            delete my_iterator; 
            return 1;  

        }
        
        bool play(string song_name, string folder_name="root"){
            
            if (!is_folder_exist(folders_names, folder_name)) {
                cout <<"the folder "<<folder_name<<" is not exist!!"<<endl;
                return 0;
            }
            else if (!is_song_exist(cont, song_name, folder_name))
            {
                cout <<"the song is not exist in "<< folder_name<<" folder" <<endl;
                
                return 0;
            }
            
            
            Iterator_type *my_iterator = cont.GetIterator();
            int size = cont.Size();
            for (int i = 0; i < size; i++)
            {
                if((my_iterator)->Current()->song == song_name && (my_iterator)->Current()->folder == folder_name) {
                    string my_song = (my_iterator)->Current()->song;
                    cout << "play "<<(my_iterator)->Current()->artist<<":"<<my_song <<endl;
                    Play_song(my_song);
                    break;
                }

                my_iterator->Next();
            }
            delete my_iterator; 
            return 1;           
        }
};

