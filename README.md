Wav music player
A simple wav music player to store and play musics.
The music player is in a data structure
of an array used in a collection and iterators
and templates.
Do the following:


Add Folder - add folder to your data struct by giving only the name of the folder you want.


Add Song - add song to a folder you created (or by defulte it's in root ) by giving the name of the song the artist of the song and the lyrics and what folder(again optional).


print Folder Songs - prints the song inside the folder you specified by giving it folder name and artist name.


Print Artist Songs - print song by a given artist name.


remove Folder - remove folder by a given folder name.


remove Song - remove song by a given song name and folder name.


Move Song - move song from folder a to b by a given song name and target folder and suorce folder


Play Song - play song by a given song name and folder name
